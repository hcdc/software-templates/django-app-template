# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "test without SILENT_HOOKS env variable" {
    run create_template
    assert_output --partial "Successfully changed"
}

@test "test with SILENT_HOOKS env variable" {
    SILENT_HOOKS=1 run create_template --no-commit
    # there should be no output
    refute_output
}

@test "test with PARENT_GIT_REPO env variable" {
    git init $BATS_TEST_TMPDIR/parent_folder
    BATS_TEST_TMPDIR=$BATS_TEST_TMPDIR/parent_folder \
      PROJECT_FOLDER=$BATS_TEST_TMPDIR \
      PARENT_GIT_REPO="../" \
      run create_template --no-commit '{use_reuse: "no"}'
    assert_success
}

@test "test without SKIP_PRE_COMMIT env variable" {
    run create_template --no-commit
    assert_output --partial "trim trailing whitespace"
}

@test "test with SKIP_PRE_COMMIT env variable" {
    # same previous test, but now with SKIP_PRE_COMMIT env variable
    SKIP_PRE_COMMIT=1 run create_template --no-commit
    refute_output --partial "trim trailing whitespace"
}
