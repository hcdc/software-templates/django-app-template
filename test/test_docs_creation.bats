# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "test creating the docs (rst)" {
    create_template '{use_markdown_for_documentation: "no"}'
    assert [ -e "${PROJECT_FOLDER}/docs/index.rst" ]
    assert [ ! -e "${PROJECT_FOLDER}/docs/index.md" ]
    setup_venv
    make -C ${PROJECT_FOLDER} test-docs
}

@test "test creating the docs (markdown)" {
    create_template '{use_markdown_for_documentation: "yes"}'
    assert [ ! -e "${PROJECT_FOLDER}/docs/index.rst" ]
    assert [ -e "${PROJECT_FOLDER}/docs/index.md" ]
    setup_venv
    make -C ${PROJECT_FOLDER} test-docs
}
