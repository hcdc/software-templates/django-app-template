"""Test file for imports."""

def test_package_import():
    """Test the import of the main package."""
    import {{ cookiecutter.package_folder }}  # noqa: F401
