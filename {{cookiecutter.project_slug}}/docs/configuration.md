(configuration)=

# Configuration options

## Configuration settings

The following settings in your djangos `settings.py` have an effect on the app:

```{eval-rst}
.. automodulesumm:: {{ cookiecutter.package_folder }}.app_settings
    :autosummary-no-titles:
```
