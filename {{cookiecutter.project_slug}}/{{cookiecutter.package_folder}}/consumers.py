{%- if cookiecutter.include_channels == "yes" %}
"""
Consumers for the {{ cookiecutter.project_slug }} app.

See Also
--------
https://channels.readthedocs.io/en/stable/topics/consumers.html
"""

from channels.generic.websocket import JsonWebsocketConsumer  # noqa: F401

{%- endif %}