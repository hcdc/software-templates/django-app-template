{%- if cookiecutter.include_channels == "yes" %}
"""
Websocket routing configuration for the {{ cookiecutter.project_slug }} app.

It exposes the `websocket_urlpatterns` list, a list of url patterns to be used
for deployment.

See Also
--------
https://channels.readthedocs.io/en/stable/topics/routing.html
"""

from typing import List, Any

from django.urls import path
from {{ cookiecutter.package_folder }} import app_settings

websocket_urlpatterns: List[Any] = [
    path(
        app_settings.{{cookiecutter.package_folder | upper }}_WEBSOCKET_URL_ROUTE,
        [
            # routes of the {{ cookiecutter.project_slug }} app
        ],
    )
]
{% endif %}