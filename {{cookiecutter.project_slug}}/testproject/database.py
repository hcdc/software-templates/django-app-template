from __future__ import annotations

from typing import Dict, TYPE_CHECKING
from pathlib import Path

if TYPE_CHECKING:
    import environ


BASE_DIR = Path(__file__).resolve().parent.parent

engines = {
    "sqlite": "django.db.backends.sqlite3",
    "postgresql": "django.db.backends.postgresql",
    "mysql": "django.db.backends.mysql",
}


def config(env: environ.Env) -> Dict:
    service_name = (
        env("DATABASE_SERVICE_NAME", default="").upper().replace("-", "_")
    )
    if service_name:
        engine = engines.get(
            env("DATABASE_ENGINE", default="sqlite"), engines["sqlite"]
        )
    else:
        engine = engines["sqlite"]
    name = env(f"{service_name}_DATABASE", default="")
    if not name and engine == engines["sqlite"]:
        name = BASE_DIR / "db.sqlite3"
    elif not name:
        name = "django_db"
    return {
        "ENGINE": engine,
        "NAME": name,
        "USER": env(f"{service_name}_USER", default="django_user"),
        "PASSWORD": env(f"{service_name}_PASSWORD", default="changeme!"),
        "HOST": env(f"{service_name}_SERVICE_HOST", default="localhost"),
        "PORT": env(f"{service_name}_SERVICE_PORT", default="5432"),
    }
